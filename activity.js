/*====================Number 01================*/


db.fruits.aggregate([
		{$match : {onSale : true}},
		{$count : "fruitsOnSale"}
]);



/*==================Number 02==============*/



db.fruits.aggregate([
		{$match : {stock : {$gte: 20}}},
		{$count : "enoughStocks"}
]);



/*===============Number 03===================*/


db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}}

	]);



/*=============Number 04====================*/



db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", max_price: {$max: "$price" }}}

	]);



/*==========Number 05=====================*/


db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", min_price: {$min: "$price" }}}

	]);